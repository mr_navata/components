import React from "react";
import * as MaterialUICore from "@material-ui/core";
import * as MaterialUIIcons from "@material-ui/icons";

export { MaterialUICore, MaterialUIIcons };
